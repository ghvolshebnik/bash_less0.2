#!/bin/bash

set -e

for i in `find . -type d -not -path "."`;
do
  echo "228.com 228.ru" > $i/file.txt
done

find . -type f -name "file.txt" | xargs sed -i 's/228\.com/229\.com/g'
